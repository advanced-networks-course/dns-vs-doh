import os
# import socket
import time
import subprocess
from random import choice, randint
from string import ascii_lowercase

AMOUNT = 1000
BULK = 100
FILE_NULL = open(os.devnull, 'w')
MIN_NUMBER_MAIN = 10
MAX_NUMBER_MAIN = 25
MIN_EXTENSION = 2
MAX_EXTENSION = 6


def dns(domain):
    subprocess.run(["nslookup", domain], stdout=FILE_NULL, stderr=FILE_NULL)


def doh(domain):
    subprocess.run(["nslookup", domain, "127.0.0.1"], stdout=FILE_NULL, stderr=FILE_NULL)


def latency(domains, method):
    subprocess.run(["ipconfig", "/flushdns"])
    sum_latency = 0.0
    latencies = {}
    index = 0
    for domain in domains:
        dns_start = time.time()
        method(domain)
        dns_end = time.time()
        packet_latency = dns_end - dns_start
        latencies[domain] = packet_latency
        sum_latency += packet_latency
        index += 1
        if index % BULK == 0:
            print(domain)
            print(packet_latency)
            print("")
    print("avg = " + str((sum_latency / float(AMOUNT))))
    return latencies


def generate_not_existing_domains():
    not_exists = []
    for x in range(0, AMOUNT):
        main_str = "".join(choice(ascii_lowercase) for i in range(randint(MIN_NUMBER_MAIN, MAX_NUMBER_MAIN)))
        for j in range(0, randint(1, 2)):
            extension = "".join(choice(ascii_lowercase) for i in range(randint(MIN_EXTENSION, MAX_EXTENSION)))
            main_str += "." + extension
        not_exists.append(main_str)
    with open("./not_exists.txt", "w") as not_exists_file:
        not_exists_file.writelines("\n".join(not_exists))


def main():
    with open("./top.txt", "r") as top_file:
        domains = [d.strip() for d in top_file.readlines()]
        sub_domains = domains[0:AMOUNT]
        dns_latency = latency(sub_domains, dns)
        doh_latency = latency(sub_domains, doh)

    with open("./results.tsv", "w") as results_file:
        for domain in domains[0:AMOUNT]:
            results_file.write("{domain}\t{dns}\t{doh}\n".format(domain=domain, dns=dns_latency[domain], doh=doh_latency[domain]))


if __name__ == "__main__":
    main()
